(function() {
    function a(a) {
        return a.dataset.identifier || (a.dataset.identifier = new Date().getTime()), a.classList.contains("expanded") ? (a.classList.remove("expanded"), 0) : (a.classList.add("expanded"), 1)
    }

    function b(b, d) {
        let e = b.target.classList.contains("expand-trigger") || b.target.classList.contains("og-close"),
            f = b.target;
        for (; !e && !f.classList.contains("event-container");) {
            let a = f.parentNode;
            f.classList.contains("expand-trigger") && (e = !0), f = a
        }
        if (e)
            if (null === c) a(d), c = d;
            else {
                let b = c.dataset.identifier === d.dataset.identifier;
                b || c.classList.remove("expanded"), setTimeout(function() {
                    c = 1 === a(d) ? d : null
                }, b ? 0 : 500)
            }
    }
    let c = null;
    (function() {
        let a = document.getElementsByClassName("event-card"),
            c = a.length;
        for (let d = 0; d < c; d++) a[d].addEventListener("click", function(a) {
            b(a, this)
        })
    })()
})();